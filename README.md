Installation steps; 
1. git clone https://navas_ca@bitbucket.org/navas_ca/global.git
2. Create .env file and copy .env.local to it. Then configure the db connection. 
3. Run composer install command
4. Run php artisan migrate --seed 
5. Run php artisan serve to run the code 