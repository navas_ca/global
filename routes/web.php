<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers as CTRL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', [CTRL\HomeController::class, 'index'])->name('home');

Route::get('/home', [CTRL\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => 'auth'], function ($request) {
	Route::group(['middleware' => 'isUser'], function ($request) {
		Route::resource('product', CTRL\ProductController::class)->except([
    			'index', 'show'
			]);;

		Route::get('my-products', [CTRL\ProductController::class, 'myProducts'])->name('my-products');
	});

	Route::group(['middleware' => 'isAdmin'], function ($request) {
		Route::get('all-products/{user_id?}', [CTRL\ProductController::class, 'allProducts'])->name('all-products');

		Route::get('users', [CTRL\UserController::class, 'usersList'])->name('users.list');
	});
});