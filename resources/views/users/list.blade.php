@extends('layouts.app')

@section('content')
	<div class="container">		
		<table class="table table-bordered">
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Created at</th>
				<th></th>
			</tr>
			@foreach($users as $key => $user)
			    <tr>
			    	<td>{{ $key+1 }}</td>
			    	<td>{{ $user->name }}</td>
			    	<td>{{ $user->created_at }}</td>
			    	<th>
			    		<a href="{{ route('all-products', ['user_id'=>$user->id]) }}">View Products</a>
			    	</th>
			    </tr>
			@endforeach
		</table>
	</div>	
@endsection
