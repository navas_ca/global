@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                

                @php
                    $users = \App\Models\User::all();                    
                @endphp

                <table class="table table-border">
                    <tr>
                        <th colspan="3">Login Details:</th>
                    </tr>                    
                    <tr>
                        <th>Username</th>
                        <th>Role</th>
                        <th>Password</th>
                    </tr>
                    @foreach($users as $user) 
                        <tr>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role }}</td>
                            <td>{{ "password" }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
