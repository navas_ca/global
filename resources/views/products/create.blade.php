@extends('layouts.app')

@section('content')
	<div class="container">		

		<form action="{{ url()->route('product.store') }}" method="post" enctype="multipart/form-data">
			@csrf
			<div class="form-group">
				<label for="">Title</label>
				<input class="form-control" type="text" name="title" value="{{ old('title') ? : '' }}">
			</div>
			<div class="form-group">
				<label for="">Details</label>
				<textarea class="form-control" name="details" id="">{{ old('details') ?: '' }}</textarea>
			</div>
			<div class="form-group">
				<label for="">Image</label>
				<input class="form-control" type="file" name="image">
			</div>
			<div class="form-group">
				<button class="btn btn-primary">Add product</button>
				<a href="{{ url()->route('my-products') }}" class="btn btn-default">Back</a>
			</div>
		</form>
	</div>	
@endsection
