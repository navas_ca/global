@extends('layouts.app')

@section('content')
	<div class="container">	

		 
		<form action="{{ url()->route('product.update', ['product'=>$product->id]) }}" method="post" enctype="multipart/form-data">
			@csrf
			{{ method_field('PATCH') }}
			<div class="form-group">
				<label for="">Title</label>
				<input class="form-control" type="text" name="title" value="{{ old('title') ? : $product->title }}">
			</div>
			<div class="form-group">
				<label for="">Details</label>
				<textarea class="form-control" name="details" id="">{{ old('details') ?: $product->details }}</textarea>
			</div>
			<div class="form-group">
				<label for="">Image</label>
				<img src="{{ asset('storage/images/'.$product->image) }}" width="75" alt="">
				<input class="form-control" type="file" name="image">
			</div>
			<div class="form-group">
				<button class="btn btn-primary">Update</button>
				<a href="{{ url()->route('my-products') }}" class="btn btn-default">Back</a>
			</div>
		</form>
	</div>	
@endsection
