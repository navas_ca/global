@extends('layouts.app')

@section('content')
	<div class="container">
		<div>
			<a class="btn btn-warning" href="{{ route('product.create') }}">{{ __('Create Products') }}</a>
		</div>	<br>

		<table class="table table-bordered">
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Image</th>
				<th>Details</th>
				<th></th>
				<th></th>
			</tr>
			@if ($products->count())
				@foreach($products as $key => $product)
				    <tr>
				    	<td>{{ $key+1 }}</td>
				    	<td>{{ $product->title }}</td>
				    	<td><img src="{{ asset('storage/images/' . $product->image) }}" alt="" width="75"></td>
				    	<td>{{ $product->details }}</td>
				    	<td><a href="{{ route('product.edit', ['product'=>$product->id]) }}">Edit</a></td>
				    	<td>
				    		<a class="btn btn-danger btn-sm" onclick="deleteProduct('{{ route('product.destroy', ['product'=>$product->id])}}')">Delete</a>			    		
				    	</td>
				    </tr>
				@endforeach
			@else
				<tr>
					<td colspan="6">No products found</td>
				</tr>
			@endif
		</table>
	</div>	

	<script>
		function deleteProduct(url) {
			$.ajax({
                url: url,
                type: 'delete',
                data: {'_token': '{{ csrf_token()}}'},
                success: function( data, textStatus ){
                	console.log(data, textStatus)
                    location.reload()
                },
                error: function( jqXhr, textStatus, errorThrown ){
                	// console.log(jqXhr, textStatus, errorThrown)
                    alert('Product deleting failed')
                    location.reload()
                }
            });
		}
	</script>
@endsection
