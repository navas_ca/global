@extends('layouts.app')

@section('content')
	<div class="container">		
		<table class="table table-bordered">
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>User</th>
				<th>Image</th>
				<th>Details</th>
			</tr>
			@if ($products->count())
				@foreach($products as $key => $product)
				    <tr>
				    	<td>{{ $key+1 }}</td>
				    	<td>{{ $product->title }}</td>
				    	<td>{{ $product->user->name }}</td>
				    	<td><img src="{{ asset('storage/images/' . $product->image) }}" alt="" width="75"></td>
				    	<td>{{ $product->details }}</td>
				    </tr>
				@endforeach
			@else
				<tr>
					<td colspan="5">No products found</td>
				</tr>
			@endif
		</table>
	</div>	
@endsection
