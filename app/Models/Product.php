<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Product extends Model
{
    use HasFactory;    

    protected $fillable = [
        'title',
        'details',
        'image',
        'user_id'
    ];

    public function scopeByUser($qry, $user_id) {
    	return $qry->where('user_id', $user_id);
    }

    public function user() {
    	return $this->belongsTo(User::class);
    }
}
