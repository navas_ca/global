<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (! $request->user() || $request->user()->role != 'admin') {
            return abort(403);
            // return $request->expectsJson()
                    // ? abort(403, 'Your are not authorised to view thsi.')
                    // : Redirect::guest(URL::route($redirectToRoute ?: 'verification.notice'));
        }
        return $next($request);
    }
}
