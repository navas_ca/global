<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class UserController extends Controller
{
    //
    public function usersList() {
    	$users = User::ByRole('user')->latest()->get();
    	return view('users.list', compact('users'));
    }
}
