<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use \Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public $user;
    public function __construct(){
        $this->middleware(function($req, $next){
            $this->user = Auth::user();
            return $next($req);
        });
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|max:255',
            'details' => 'required',
            'image' => 'required|mimes:jpg,bmp,png'
        ]);

        if ($request->hasFile('image')) {                
            $file_name = Str::random(4) . time() . "." . $request->file('image')->extension();
            Storage::putFileAs(
                'public/images', $request->file('image'), $file_name
            );
        }

        $product = $this->user->products()->create([
            'title' => $request->title,
            'details' => $request->details,
            'image' => $file_name,
            'user_id' => $this->user->id,
        ]);

        $request->session()->flash('success', 'Product added successfully!');

        return redirect()->route('my-products');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        if ($product->user()->is($this->user)) {
            return view('products.edit')->with(['product'=>$product]);
        }
        else {
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validated = $request->validate([
            'title' => 'required|max:255',
            'details' => 'required',
            'image' => 'nullable|mimes:jpg,bmp,png'
        ]);
        if ($product->user()->is($this->user)) {
            $product->title = $request->title;
            $product->details = $request->details;
            if ($request->hasFile('image')) {                
                $file_name = Str::random(4) . time() . "." . $request->file('image')->extension();
                Storage::putFileAs(
                    'public/images', $request->file('image'), $file_name
                );
                Storage::delete('public/images/'.$product->image);
                $product->image = $file_name;
            }
            $product->save();
            $request->session()->flash('success', 'Product updated successfully!');
            return redirect()->route('product.edit', ['product' => $product->id]);
        }
        else {
            return abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if ($product->user()->is($this->user)) {
            Storage::delete('public/images/'.$product->image);
            $product->delete();
            session()->flash('success', 'Product deleted successfully!');
            return response()->json(['message'=>'Product deleted successfully']);
        }
        else {
            return abort(403);
        }
    }

    public function myProducts()
    {
        $products = Product::ByUser($this->user->id)->latest()->get();
        return view('products.list')->with(['products'=>$products]);
    }

    public function allProducts($user_id=null) {
        $products = Product::when($user_id, function($qry) use ($user_id){
                                $qry->where('user_id', $user_id);
                            })
                            ->latest()->get();
        return view('products.all-list')->with(['products'=>$products]);
    }
}
